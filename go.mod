module github.com/aviau/gopass

go 1.15

require (
	github.com/boombuler/barcode v1.0.1 // indirect
	github.com/mattn/go-colorable v0.1.11 // indirect
	github.com/mgutz/ansi v0.0.0-20200706080929-d51e80ef957d
	github.com/pquerna/otp v1.3.0
	github.com/stretchr/testify v1.7.0
	golang.org/x/sys v0.0.0-20211007075335-d3039528d8ac // indirect
	golang.org/x/term v0.0.0-20210927222741-03fcf44c2211
	honnef.co/go/tools v0.2.1
)
